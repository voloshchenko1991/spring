package com.dvo.spring;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExternalClass {
    private List<Internal> internalList;
}
