package com.dvo.spring;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InternalClass implements Internal{
    private String userName;
    private String userAddress;
    private String userPhone;
    private int userBalance;

    public void print() {
        System.out.println("InternalClass: \n\n" + userName + "\n" + userAddress + "\n" + userPhone + "\n" + userBalance);
    }
}
