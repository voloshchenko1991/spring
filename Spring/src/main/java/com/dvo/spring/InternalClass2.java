package com.dvo.spring;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InternalClass2 implements Internal {
    private String userName;
    private int userID;
    public void print() {
        System.out.println("InternalClass2\n\n"+userName + "\n" + userID);
    }
}
