package com.dvo.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("main.xml");
        ExternalClass externalClass = (ExternalClass) applicationContext.getBean("externalBean");
        externalClass.getInternalList().forEach(Internal::print);

    }
}
